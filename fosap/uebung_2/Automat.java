package fosap.uebung_2;

/**
 * Stellt einen DFA mit Start und aktuellen Zustand dar.
 * <p>
 * Created by Marc Schmidt on 13.05.2017.
 */
public class Automat {

	private State mStart;
	private State mCur;

	public Automat(State pStart) {
		mStart = pStart;
	}

	/**
	 * Wechselt die Zustände des Automaten für ein eingegebenes Wort.
	 */
	public String feed(String pFeed) {
		mCur = mStart;

		String output = "-->" + mCur;

		for (int i = 0; i < pFeed.length(); i++) {
			output += " -" + pFeed.charAt(i) + "-> ";
			if (!transform(pFeed.charAt(i))) {
				return output + " failed";
			}
			output += mCur;
		}

		return output + " end";
	}

	private boolean transform(char pFeed) {
		State tmp = mCur.transform(pFeed);
		if (tmp != null) {
			mCur = tmp;
			return true;
		}
		return false;
	}
}

package fosap.uebung_2;

/**
 * Created by Marc Schmidt on 13.05.2017.
 */
public class VerifyAutomat {

	/**
	 * Eingaben die überprüft werden
	 */
	private static final String[] CHECK = {"UDRAZRAZAZRRAZURURURAZAZRRAZARRRZARZRRRAZAZRZAZARRZAZDAZAZUDURAZDRURAZRAZRRAZ", "AZUUDRAZUDUARZUAZARRZ",
			"AZRUAZUAZUAZDAZUARRRRRZARZDAZARZARRRZUARZAZDUAZARRZRAZARZAZRRRRDDARRZRARZAZAZAZUDRRAZARZARZAZUAZARZUARRZDARRZRUDDUAZURRDRARRZDARZARZRAZDARZARZAZUARRZURARZARZUDARZAZARZARRZUARRZRARZAZAZAZARZARZDARZUDAZAZUDARZARRZARZAZAZAZUDUARRRZAZRRAZDRURA", "UAZDUARRRRZAZUDUDDRRUDARRRZURRARRZAZURRAZAZDAZARZRUDRARRRRRZRAZUDARZUUDDARZUARZDUDUDARZUAZUARZARZURAZRARZAZAZDRDDRRRAZUARRZURRDAZAZURAZARZARZARZRARZAZARRZRARRRRRRZARZAZARZRDRDARRRZAZAZARZUARZUDAZAZARZUARRZAZRAZDARZARRZDDARRZARZAZARZURARZRDURAZRUDAZARZAZRDUARZURARZAZDUAZARZARRRRRZARRZAZAZDUUARZAZRA"};
	private static final Character UP = 'U';
	private static final Character DOWN = 'D';
	private static final Character OPEN = 'A';
	private static final Character CLOSE = 'Z';
	private static final Character ALARM = 'R';

	private static Automat mAutomat;

	public static void main(String[] args) {
		buildAutomat();

		for (String str : CHECK) {
			System.out.println(mAutomat.feed(str));
		}
	}

	/**
	 * Erstellt den Automaten
	 */
	private static void buildAutomat() {
		State notr = new State("Notruf");
		State g0 = new State("Erdgeschoss (Zu)");
		State g1 = new State("Geschoss 1 (Zu)");
		State g2 = new State("Geschoss 2 (Zu)");
		State g3 = new State("Geschoss 3 (Zu)");
		State g4 = new State("Geschoss 4 (Zu)");
		State g0O = new State("Erdgeschoss (Auf)");
		State g1O = new State("Geschoss 1 (Auf)");
		State g2O = new State("Geschoss 2 (Auf)");
		State g3O = new State("Geschoss 3 (Auf)");
		State g4O = new State("Geschoss 4 (Auf)");

		//Hochfahren
		g0.add(UP, g1);
		g1.add(UP, g2);
		g2.add(UP, g3);
		g3.add(UP, g4);

		//Runterfahren
		g4.add(DOWN, g3);
		g3.add(DOWN, g2);
		g2.add(DOWN, g1);
		g1.add(DOWN, g0);

		//Notruf
		notr.add(ALARM, notr);
		g0.add(ALARM, notr);
		g1.add(ALARM, notr);
		g2.add(ALARM, notr);
		g3.add(ALARM, notr);
		g4.add(ALARM, notr);
		g0O.add(ALARM, notr);
		g1O.add(ALARM, notr);
		g2O.add(ALARM, notr);
		g3O.add(ALARM, notr);
		g4O.add(ALARM, notr);

		//Notruf
		notr.add(ALARM, notr);
		notr.add(UP, notr);
		notr.add(DOWN, notr);
		notr.add(CLOSE, notr);
		notr.add(OPEN, notr);

		//Türen zu
		g0O.add(CLOSE, g0);
		g1O.add(CLOSE, g1);
		g2O.add(CLOSE, g2);
		g3O.add(CLOSE, g3);
		g4O.add(CLOSE, g4);

		//Türen auf
		g0.add(OPEN, g0O);
		g1.add(OPEN, g1O);
		g2.add(OPEN, g2O);
		g3.add(OPEN, g3O);
		g4.add(OPEN, g4O);

		mAutomat = new Automat(g0);
	}
}

package fosap.uebung_2;

import java.util.HashMap;

/**
 * Speichert einen Zustand eines Automaten mit den dazugehörigen Übergangsfunktionen für die jeweiligen Eingaben.
 * <p>
 * Created by Marc Schmidt on 13.05.2017.
 */
public class State {

	/**
	 * Speichert die Übergangsfunktionen
	 */
	private HashMap<Character, State> mFunction = new HashMap<>();
	/**
	 * Name des Zustands
	 */
	private String mName;
	/**
	 * Speichert ob der Zustand ein Endzustand ist
	 */
	private boolean mEndState;

	public State(String pName) {
		mName = pName;
		mEndState = true;
	}

	/**
	 * Hinzufügen eine Übergangsfunktion
	 */
	public State add(Character pFeed, State pState) {
		mFunction.put(pFeed, pState);
		return this;
	}

	/**
	 * Geht entsprechend der Eingabe in den nächsten Zustand über
	 */
	public State transform(Character pFeed) {
		return mFunction.get(pFeed);
	}

	@Override
	public String toString() {
		return mName;
	}
}

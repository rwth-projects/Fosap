package fosap.uebung_1;

/**
 * Created by Marc Schmidt on 06.05.2017.
 */
public class H2 {

	private static final String REGEX = "A((B|C|(CB(CB)*(|C))|(BC(BC)*(|B)))A)*";
	/**
	 * Pfade die gematched werden sollen
	 */
	private static final String[] PATHS = {"ABCA", "A", "ABA", "ACBCBCBCA"};
	/**
	 * falsche Pfade
	 */
	private static final String[] NO_PATHS = {"ABBA", "AA"};

	public static void main(String[] args) {
		System.out.println("match");

		for (String path : PATHS) {
			System.out.println(path + '\t' + path.matches(REGEX));
		}
		System.out.println("no match");
		for (String path : NO_PATHS) {
			System.out.println(path + '\t' + path.matches(REGEX));
		}
	}

}

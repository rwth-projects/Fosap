package fosap.uebung_4;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class MyNFA {

	static public void main(String args[]) {
		Set<Integer> states = new HashSet<>();
		states.add(1);
		states.add(2);
		states.add(3);
		states.add(4);
		NFA<Integer, String> M = new NFA<>(states);
		M.addTransition(1, "rechts", 2);
		M.addTransition(1, "runter", 3);
		M.addTransition(2, "links", 1);
		M.addTransition(2, "runter", 4);
		M.addTransition(3, "rechts", 4);
		M.addTransition(3, "hoch", 1);
		M.addTransition(4, "links", 3);
		M.addTransition(4, "hoch", 2);
		List<String> eingabe = new LinkedList<>();
		eingabe.add("rechts");
		eingabe.add("runter");
		eingabe.add("links");
		Set<Integer> reachable = M.simulate(1, eingabe);
		System.out.println(reachable);
	}
}
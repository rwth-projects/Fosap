package fosap.uebung_4;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Stellt einen Zustand eines NFAs seinen dazugehörigen Transitionen dar. Der generische Typ {@code S} steht für Typ der Zustände und der
 * generische Typ {@code A} für den den Typ des Alphabets.
 * <p>
 * Erstellt von Daniel Sous (367726) und Marc Schmidt (367744) am 27.05.2017.
 */
public class State<S, A> {

	private HashMap<A, List<S>> mTransistions = new HashMap<>();

	/**
	 * Fügt für das gegebene Symbol {@code pSymbol} die gegebene Transition {@code pTransition} hinzu.
	 *
	 * @param pSymbol     Eingabe-Symbol
	 * @param pTransition Transition
	 */
	public void addTransition(A pSymbol, S pTransition) {
		List<S> list = mTransistions.get(pSymbol);
		if (list == null) {
			list = new LinkedList<>();
			list.add(pTransition);
			mTransistions.put(pSymbol, list);
		} else {
			list.add(pTransition);
		}
	}

	/**
	 * Gibt die aus dem Zustand, für das Eingabe Symbol {@code pSymbol}, erreichbaren Zustände zurück.
	 *
	 * @param pSymbol Symbol dass eingegebenen wird
	 * @return erreichbare Zustände. {@code null} falls keine Transition für das gegebene Symbol existiert
	 */
	public List<S> simulate(A pSymbol) {
		return mTransistions.get(pSymbol);
	}
}

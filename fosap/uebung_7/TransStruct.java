package fosap.uebung_7;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by Marc Schmidt on 02.07.2017.
 */
public class TransStruct<S, A> {

	private HashMap<Transition<S, A>, LinkedList<LinkedList<Transition<S, A>>>> mMap = new HashMap<>();

	public void add(Transition<S, A> pTransition, LinkedList<Transition<S, A>> pTransitions) {
		LinkedList<LinkedList<Transition<S, A>>> parentList = mMap.get(pTransition);
		if (parentList == null) {
			parentList = new LinkedList<>();
			mMap.put(pTransition, parentList);
		} else if (parentList.contains(pTransitions)) {
			return;
		}
		parentList.add(pTransitions);
	}

	@Override
	public String toString() {
		String text = "";
		for (Map.Entry<Transition<S, A>, LinkedList<LinkedList<Transition<S, A>>>> entry : mMap.entrySet()) {
			text += "B(" + entry.getKey() + ") ";
			for (LinkedList<Transition<S, A>> transitions : entry.getValue()) {
				text += transitions + "\t";
			}
			text += Production.LINE_SEP;
		}
		return text;
	}
}

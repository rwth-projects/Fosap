package fosap.uebung_7;

import java.util.*;

/**
 * Created by Marc Schmidt on 02.07.2017.
 */
public class Aufgabe18 {

	private static SatNFA<String, String> sNFA;
	private static LinkedList<Production> sProductions;
	private static TransStruct<String, String> sTransStruct = new TransStruct<>();

	public static void main(String[] args) {
		if (args.length == 0) {
			init("1,2,3", "1;a;2,2;b;3", "S->aA,S->Bb,S->E,A->Sb,B->aS");
		} else if (args.length == 3) {
			init(args[0], args[1], args[2]);
		} else {
			throw new IllegalArgumentException("Eingabe vom falschen Format");
		}

		saturate(sNFA, sProductions);

		printRes(sNFA);
	}

	private static void init(String pStates, String pTransitions, String pProductions) {
		String[] parts = pStates.split(",");
		Set<String> states = new HashSet<>(Arrays.asList(parts));

		sNFA = new SatNFA<>(states);
		parts = pTransitions.split(",");
		for (String part : parts) {
			String[] tmp = part.split(";");
			sNFA.addTransition(tmp[0], tmp[1], tmp[2]);
		}

		sProductions = new LinkedList<>();
		parts = pProductions.split(",");
		for (String part : parts) {
			sProductions.add(new Production(part));
		}
	}

	private static void saturate(SatNFA<String, String> pNFA, LinkedList<Production> pProducts) {
		for (Production product : pProducts) {
			if (product.isEpsilon()) {
				Set<String> states = pNFA.getStates().keySet();
				for (String state : states) {
					pNFA.addTransition(state, product.getLeftSide(), state);

					Transition<String, String> self = new Transition<>(state, state, Production.EPSILON);
					LinkedList<Transition<String, String>> list = new LinkedList<>();
					list.add(self);
					sTransStruct.add(self, list);
				}
			}
		}

		int changed = 1;
		while (changed > 0) {
			changed = 0;

			List<SatState<String, String>> states = sNFA.createStates();
			for (Production product : pProducts) {
				for (SatState<String, String> state : states) {
					System.out.println("state: " + state);
				}
			}
		}
	}

	private static void printRes(SatNFA<String, String> pNFA) {
		String output = "given NFA:" + Production.LINE_SEP + pNFA + Production.LINE_SEP;
		output += "Datenstruktur B:" + Production.LINE_SEP + sTransStruct;
		output += Production.LINE_SEP + "example transition: o-p->q, für Automat geht von Zustand o mit der Transition p in den Zustand q";

		System.out.println(output);
	}
}

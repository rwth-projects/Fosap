package fosap.uebung_7;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Stellt einen Zustand eines NFAs seinen dazugehörigen Transitionen dar. Der generische Typ {@code S} steht für Typ der Zustände und der
 * generische Typ {@code A} für den den Typ des Alphabets.
 * <p>
 * Erstellt von Daniel Sous (367726) und Marc Schmidt (367744) am 27.05.2017.
 */
public class SatState<S, A> {

	private HashMap<A, List<S>> mTransistions = new HashMap<>();
	private S mName;

	public SatState(S pName) {
		mName = pName;
	}

	/**
	 * Fügt für das gegebene Symbol {@code pSymbol} die gegebene Transition {@code pState} hinzu.
	 *
	 * @param pSymbol Eingabe-Symbol
	 * @param pState  Transition
	 */
	public boolean addTransition(A pSymbol, S pState) {
		List<S> list = mTransistions.get(pSymbol);
		if (list == null) {
			list = new LinkedList<>();
			mTransistions.put(pSymbol, list);
		}
		if (list.contains(pState)) {
			return false;
		} else {
			list.add(pState);
			return true;
		}
	}

	/**
	 * Gibt die aus dem Zustand, für das Eingabe Symbol {@code pSymbol}, erreichbaren Zustände zurück.
	 *
	 * @param pSymbol Symbol dass eingegebenen wird
	 * @return erreichbare Zustände. {@code null} falls keine Transition für das gegebene Symbol existiert
	 */
	public List<S> simulate(A pSymbol) {
		return mTransistions.get(pSymbol);
	}

	@Override
	public boolean equals(Object pO) {
		if (this == pO) { return true; }
		if (pO == null || getClass() != pO.getClass()) { return false; }

		SatState<?, ?> satState = (SatState<?, ?>) pO;

		if (mTransistions != null ? !mTransistions.equals(satState.mTransistions) : satState.mTransistions != null) { return false; }
		return mName != null ? mName.equals(satState.mName) : satState.mName == null;
	}

	@Override
	public int hashCode() {
		int result = mTransistions != null ? mTransistions.hashCode() : 0;
		result = 31 * result + (mName != null ? mName.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		String text = "";
		for (Map.Entry<A, List<S>> entry : mTransistions.entrySet()) {
			List<S> list = entry.getValue();
			for (S s : list) {
				text += mName + "-" + entry.getKey() + "->" + s + Production.LINE_SEP;
			}
		}
		return text;
	}

	public boolean hasProduction(Production pProduction){

		return false;
	}

	public S getName() {
		return mName;
	}
}

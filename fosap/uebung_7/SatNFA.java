package fosap.uebung_7;

import java.util.*;

/**
 * Simuliert einen SatNFA. Der generische Typ {@code S} steht für Typ der Zustände und der generische Typ {@code A} für den den Typ des Alphabets.
 * <p>
 * Erstellt von Daniel Sous (367726) und Marc Schmidt (367744) am 27.05.2017.
 */
public class SatNFA<S, A> {

	/**
	 * {@code HashMap} die die einzelnen Zuständen beinhaltet.
	 */
	private HashMap<S, SatState<S, A>> mStates = new HashMap<>();

	/**
	 * Fügt die übergebenen Zustände dem SatNFA hinzu. Dabei kann jeder Zustand nur einmal hinzugefügt werden.
	 *
	 * @param pStates Zustände die hinzugefügt werden
	 */
	public SatNFA(Set<S> pStates) {
		for (S state : pStates) {
			if (!mStates.containsKey(state)) {
				mStates.put(state, new SatState<>(state));
			}
		}
	}

	/**
	 * Fügt eine Transition vom gegebenen Zustand {@code q} zum gegebenen Zustand {@code p} mit dem Symbol {@code a} hinzu.
	 *
	 * @param q Zustand indem gestartet wird
	 * @param a Symbol für die Transition
	 * @param p Zustand in dem geendet wird
	 * @throws IllegalArgumentException wenn der übergebene Zustand {@code q} nicht im SatNFA enthalten ist
	 */
	public boolean addTransition(S q, A a, S p) {
		SatState<S, A> state = mStates.get(q);
		if (state == null) {
			throw new IllegalArgumentException("übergebener Zustand q ist nicht im SatNFA enthalten");
		}

		return state.addTransition(a, p);
	}

	@Override
	public boolean equals(Object pO) {
		if (this == pO) { return true; }
		if (pO == null || getClass() != pO.getClass()) { return false; }

		SatNFA<?, ?> satNFA = (SatNFA<?, ?>) pO;

		return mStates != null ? mStates.equals(satNFA.mStates) : satNFA.mStates == null;
	}

	@Override
	public int hashCode() {
		return mStates != null ? mStates.hashCode() : 0;
	}

	@Override
	public String toString() {
		String text = "states:" + Production.LINE_SEP + '{';
		Set<S> states = mStates.keySet();
		int count = 0;
		for (S state : states) {
			count++;
			text += state;
			if (count < states.size()) {
				text += ",";
			}
		}
		text += '}' + Production.LINE_SEP + "transitions:" + Production.LINE_SEP;
		for (SatState<S, A> state : mStates.values()) {
			text += state;
		}

		return text;
	}

	/**
	 * Gibt die Zustände zurück, in denen sich der SatNFA nach durchlaufen des gegebenen Wortes befindet. Dabei startet er in Zustand {@code q}.
	 *
	 * @param q Zustand in dem die Simulation startet
	 * @param w Wort dass eingelesen wird
	 * @return erreichbaren Zustände für das gegebene Wort
	 */
	public Set<S> simulate(S q, List<A> w) {
		// Zustände in denen der SatNFA aktuell sein kann, die also für weitere Transitionen verwendet werden
		HashSet<S> curStates = new HashSet<>();
		curStates.add(q);

		for (A word : w) { // iterieren über das Wort
			HashSet<S> nextStates = new HashSet<>(); // temporäres speichern der nächsten Zustände
			for (S state : curStates) { // abbilden jedes aktuellen Zustand für das Wort word
				List<S> simulatedStates = mStates.get(state).simulate(word); // zurückgeben der Menge der Zustände, für das einlesen des Wortes word
				if (simulatedStates != null) {
					nextStates.addAll(simulatedStates);
				}
			}
			curStates = nextStates; // aktuelle Zustände updaten, nachdem für alle das Wort w eingelesen wurde
		}

		return curStates;
	}

	public HashMap<S, SatState<S, A>> getStates() {
		return mStates;
	}

	public LinkedList<SatState<S, A>> createStates(){
		return new LinkedList<>(mStates.values());
 	}
}

package fosap.uebung_7;
import java.util.*;

public class TokenLister {

	/**
	 * @param args args[0]=Wort, args[i], i > 0 regulaere Ausdruecke r_i.
	 */
	public static void main(String[] args) {

		//Falls noch nicht einmal ein Wort uebergeben wurde...
		if (args.length < 1) {
			System.out.println("Zu wenige Parameter!\nargs[0]=(String)word, args[i], i > 0, sind die regulaeren Ausdruecke r_i.");
			return;
		}

		//Token
		LinkedList<AbstractMap.SimpleEntry<String, String>> token = new LinkedList<>();
		//Regular Expressions (r_i)
		LinkedList<String> regex = new LinkedList<>();

		//Lade das Wort und die regulaeren Audruecke r_i
		String word = args[0];
		for (int i = 1; i < args.length; i++) {
			regex.add(args[i]);
		}

		//Loese das Wort in u_j auf:
		while (word.length() > 0) {

			int longestPrefix = -1;
			int maxLength = 0;

			AbstractMap.SimpleEntry<String, String>[] longestPrefixes = new AbstractMap.SimpleEntry[3];

			//Loese das Praefix zur Sprache von r_i, r_num und r_id auf...
			longestPrefixes[0] = getR_IMax(word, regex);
			longestPrefixes[1] = getR_NumMax(word);
			longestPrefixes[2] = getR_IdMax(word);

			//...und entscheide, welches Praefix das laengste ist.
			for (int i = 0; i < 3; i++) {
				//Konnte ein Praefix fuer den jeweiligen regulaeren Ausdruck gefunden werden...
				if (longestPrefixes[i] != null) {
					//... dann merke diesen, falls er der laengste ist.
					if (longestPrefixes[i].getKey().length() > maxLength) {
						longestPrefix = i;
						maxLength = longestPrefixes[i].getKey().length();
					}
				}
			}

			//Falls der Praefix keiner Sprache von R zugeordnet werden konnte...
			if (longestPrefix == -1) {
				//... dann terminiere: FEHLER.
				break;
			} else {
				//sonst trage den laengsten Praefix als Token ein.
				token.add(longestPrefixes[longestPrefix]);
				word = word.substring(longestPrefixes[longestPrefix].getKey().length());
			}
		}

		//Falls das Wort nicht komplett aufgeloest werden konnte...
		if (word.length() > 0) {
			//... dann ist das Wort ungueltig.
			System.out.println("Fehler: Der Praefix von \"" + word + "\" kann keiner Sprache von r aus R zugeordnet werden.");
		} else {
			//... sonst war es gueltig und dann gebe die Tokens aus.
			for (int i = 0; i < token.size(); i++) {
				System.out.println(token.get(i).getKey() + "\t->\t" + token.get(i).getValue());
			}
		}
	}

	/**
	 * @param word ist das ganze Wort
	 * @param regex ist eine Liste mit den regulaeren Ausdruecken r_i
	 * @return Token mit dem laengsten Praefix der einer Sprache von r_i zugeordnet werden konnte, falls es einen gibt, sonst null.
	 */
	public static AbstractMap.SimpleEntry<String, String> getR_IMax(String word, LinkedList<String> regex) {

		boolean solved = false;     //existiert mindestens ein Regulaerer Ausdruck r_i, sodass der Praefix Teil der Sprache von r_i ist?
		int longestPrefix = -1;
		int maxLength = 0;

		for (int i = 0; i < regex.size(); i++) {
			//Falls der Praefix u_0 des Wortes Element von L(r_i)...
			if (word.startsWith(regex.get(i))) {
				//... merke dir den laengst moeglichen Praefix u_0, mit u_0 Element von L(r_i)
				if (regex.get(i).length() > maxLength) {
					longestPrefix = i;
					maxLength = regex.get(i).length();
				}
				solved = true;
			}
		}

		if (solved) {
			return new AbstractMap.SimpleEntry<>(regex.get(longestPrefix), "r_" + longestPrefix);
		} else {
			return null;
		}
	}

	/**
	 * @param word ist das ganze Wort
	 * @return Token mit dem laengsten Praefix der einer Sprache von r_num zugeordnet werden konnte, falls es einen gibt, sonst null.
	 */
	public static AbstractMap.SimpleEntry<String, String> getR_NumMax(String word) {
		int i = 0; //Anzahl von Charactern die Teil des Praefix sind, interpretiert als Teil der Sprache von r_num
		//Iteriere durch das Wort...
		while (i < word.length()) {
			if (Character.isDigit(word.charAt(i))) {
				i++;
			} else { //... bis der erste nicht-zahl char gefunden ist. Breche dann ab.
				break;
			}
		}
		//Gebe das Praefix bis zum ersten nicht-zahl char zurück.
		if (i > 0) {
			return new AbstractMap.SimpleEntry<>(word.substring(0, i), "r_num");
		} else {
			return null;
		}
	}

	/**
	 * @param word ist das ganze Wort
	 * @return Token mit dem laengsten Praefix der einer Sprache von r_id zugeordnet werden konnte, falls es einen gibt, sonst null.
	 */
	public static AbstractMap.SimpleEntry<String, String> getR_IdMax(String word) {
		int i = 0;  //Anzahl von Charactern die Teil des Praefix sind, interpretiert als Teil der Sprache von r_num
		//Iteriere durch das Wort...
		while (i < word.length()) {
			if (Character.isLowerCase(word.charAt(i))) {
				i++;
			} else {    //... bis der erste char gefunden ist, der kein kleiner Buchstabe ist. Breche dann ab.
				break;
			}
		}

		//Beginnt das Praefix nicht mit einem Buchstaben...
		if (i == 0) //... dann ist er nicht Teil von r_id. Gebe dann das leere Wort zurück.
		{
			return null;
		}

		//Setze die Iteration fort...
		while (i < word.length()) {
			//... folgen weitere kleine Buchstaben und/oder Zahlen, tue dies...
			if (Character.isDigit(word.charAt(i)) || Character.isLowerCase(word.charAt(i))) {
				i++;
			} else {//... bis das erste char gefunden wurde, welches nicht zu r_id gehört.
				break;
			}
		}
		//Gebe das Praefix bis zum ersten nicht-r_id Symbol zurück.
		return new AbstractMap.SimpleEntry<>(word.substring(0, i), "r_id");
	}
}

package fosap.uebung_7;

/**
 * Created by Marc Schmidt on 02.07.2017.
 */
public class Transition<S, A> {

	private S mState1;
	private S mState2;
	private A mTransition;

	public Transition(S pState1, S pState2, A pTransition) {
		mState1 = pState1;
		mState2 = pState2;
		mTransition = pTransition;
	}

	@Override
	public boolean equals(Object pO) {
		if (this == pO) { return true; }
		if (pO == null || getClass() != pO.getClass()) { return false; }

		Transition<?, ?> that = (Transition<?, ?>) pO;

		if (mState1 != null ? !mState1.equals(that.mState1) : that.mState1 != null) { return false; }
		if (mState2 != null ? !mState2.equals(that.mState2) : that.mState2 != null) { return false; }
		return mTransition != null ? mTransition.equals(that.mTransition) : that.mTransition == null;
	}

	@Override
	public int hashCode() {
		int result = mState1 != null ? mState1.hashCode() : 0;
		result = 31 * result + (mState2 != null ? mState2.hashCode() : 0);
		result = 31 * result + (mTransition != null ? mTransition.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return mState1 + "-" + mTransition + "->" + mState2;
	}
}

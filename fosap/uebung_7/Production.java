package fosap.uebung_7;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Marc Schmidt on 02.07.2017.
 */
public class Production {

	public static final String EPSILON = "E";
	public static final String LINE_SEP = System.lineSeparator();

	private String mLeftSide;
	private String mRightSide;

	public Production(String pContent) {
		String[] parts = pContent.split("->");
		mLeftSide = parts[0];
		mRightSide = parts[1];
	}

	@Override
	public boolean equals(Object pO) {
		if (this == pO) { return true; }
		if (pO == null || getClass() != pO.getClass()) { return false; }

		Production that = (Production) pO;

		if (mLeftSide != null ? !mLeftSide.equals(that.mLeftSide) : that.mLeftSide != null) { return false; }
		return mRightSide != null ? mRightSide.equals(that.mRightSide) : that.mRightSide == null;
	}

	@Override
	public int hashCode() {
		int result = mLeftSide != null ? mLeftSide.hashCode() : 0;
		result = 31 * result + (mRightSide != null ? mRightSide.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return mLeftSide + '\t' + mRightSide;
	}

	public boolean isEpsilon() {
		return mRightSide.equals(EPSILON);
	}

	public String getLeftSide() {
		return mLeftSide;
	}

	public String getRightSide() {
		return mRightSide;
	}

	public List<String> getTerminals() {
		LinkedList<String> list = new LinkedList<>();
		for (int i = 0; i < mRightSide.length(); i++) {
			list.add("" + mRightSide.charAt(i));
		}
		return list;
	}
}
